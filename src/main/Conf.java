package main;

import value.ExpValue;

import java.util.HashMap;
import java.util.Map;

public class Conf {

    private final Map<String, ExpValue<?>> map = new HashMap<>();

    public void update(String id, ExpValue<?> v) {
        map.put(id, v);
    }

    public ExpValue<?> obtain(String id) {
        return map.get(id);
    }

    public boolean contains(String id) {
        return map.containsKey(id);
    }
}
