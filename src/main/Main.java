package main;

import gen.*;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.InputStream;

public class Main {

    public static void main(String[] args) throws Exception {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream(args[0]);
        CharStream charStream = CharStreams.fromStream(inputStream);

        HaveFunLexer lexer = new HaveFunLexer(charStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        HaveFunParser parser = new HaveFunParser(tokenStream);

        parser.removeErrorListeners();
        parser.addErrorListener(new MyErrorListener());

        ParseTree tree = parser.prog();

        if (!(MyErrorListener.hasError)) {
            IntHaveFun interpreter = new IntHaveFun();
            interpreter.visit(tree);
        }
    }
}
